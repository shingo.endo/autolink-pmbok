'use strict';

const PMBOK_URL = chrome.extension.getURL('./PMBOKGuideSixthEd_JPN.pdf');
const PAGE_OFFSET = 38;	// "Page 1" of PMBOK is on page 39 in the PMBOK PDF.

(function replaceChildTextNodes(e) {
	// If the current node is a text node and it contains "Pxxx (x: number)"-like string, replaces its parent element.
	if (e.nodeName === '#text' && /\bP(\d+)/.test(e.nodeValue)) {
		const parent = e.parentElement;
		// Checks whether the element is not <script> or <style>.
		if (parent.tagName !== 'SCRIPT' && parent.tagName !== 'STYLE') {
			// Replaces innerHTML to add hyper links to the local PMBOK PDF.
			parent.innerHTML = parent.innerHTML.replace(/\bP(\d+)/g, (match, p1) => {
				const pageNo = PAGE_OFFSET + Number(p1);
				const url = `${PMBOK_URL}#view=Fit&page=${pageNo}`;
				return `<a href="${url}" target="_blank">${match}</a>`;
			});
		}
	}

    // Checks the child nodes recursively.
	for (let i = 0; i < e.childNodes.length; i++) {
		replaceChildTextNodes(e.childNodes[i]);
	}
})(document.body);
